--- 
title: "useR! Knowledgebase"
author: "The R Project"
date: "`r Sys.Date()`"
documentclass: book
site: bookdown::bookdown_site
biblio-style: apalike
link-citations: yes
url: 'https://rconf.gitlab.io/userknowledgebase/'
description: "Everything you need to start a useR! Conference"
favicon: assets/user-logo.png
always_allow_html: yes
split_by: "section"
sharing:
      github: false
---

# Abstract {-}

The useR! knowledgebase's goal is to build and maintain resources for organizing the useR! conferences. It was started as part of the [Google Season of Docs](https://developers.google.com/season-of-docs/docs/participants) project in 2021 [(Proposal)](https://github.com/rstats-gsod/gsod2021/wiki/useR!-knowledgebase).

Let's start with Congratulations! the fact that you have reached this knowledgebase means that you are interested in organising the [useR! conference](https://www.r-project.org/conferences/). The first international conference for the R community was [useR! 2004](https://www.r-project.org/conferences/useR-2004/), and it was held in-person at the [Technische Universität Wien](https://www.tuwien.at/en/) in Vienna, Austria, between the 20th and 22nd of May 2004. The organizing team back then was probably as excited as you are now. But they were also a bit scared and full of questions: 

1. How does this work? 
1. How do we get sponsors?
1. What is the first thing we have to do?

Years later, we can offer some advice from our cumulative experiences. Even though every event is in some ways unique, we have come to a more or less standard approach for organizing these conferences. With this [useR! knowledgebase](https://rconf.gitlab.io/userknowledgebase), we want to support you and make you feel at ease in organizing your own useR! event. Don't worry; we will be there along the way!

It is, however, recommended that everyone from the organizing team has already taken part in at least one useR! conference as an attendee before organizing one. You are more than welcome to reach out to the [Global useR!  Working Group](https://user2021.r-project.org/about/global-team/) for more information regarding how to start.
