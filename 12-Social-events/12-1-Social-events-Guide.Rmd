# Social events

The social events at useR! include:
  
+ Opening and closing ceremonies
+ my first useR! (aka the newbies event)
+ Partner community events
        + R-Ladies
        + MiR
        + LatinR
        + AfricaR
        + RUGS
+ Satellite events: potential organizations that would do satellite events.
+ Gala dinner (in in-person settings) 
+ Networking sessions, meant to creating possibilities for people to know each other
+ Recreation activities, to make a pause during the week
    + Yoga sessions
    + Creation Lab event
    + a movie day 
    + aRt Gallery event
    + tRivia



## Code of Conduct

All event organizers were asked to read and agree following the Code of Conduct before they were hired. This protects them too from unwanted behavior from the attendees or organizers.

Following the CoC is also extremely important in in-person social events. The informality and alcohol consumption makes in-person social events a space of the conference vulnerable to Code of Conduct violations. Prevent excessive offer/consumption of alcohol for this and other motives. 



## Opening and closing ceremonies

## Satellite events

## Gala dinner

## Networking sessions{#networking-sessions}

## Recreational activities {#recreational-activities}