## Portuguese version of the useR! Code of Conduct, as of useR! 2021 {#coc-pt}

Translated by Sara Mortara, member of the Code of Conduct Response Team

__CÓDIGO DE CONDUTA__

A organização de useR! 2021 é dedicada a proporcionar uma experiência de conferência segura e inclusiva para todas as pessoas independente de idade, gênero, orientação sexual, deficiência, aparência física, raça ou religião (ou a falta dela).

Todas as pessoas participantes (incluindo as pessoas organizadoras, ouvintes, palestrantes, patrocinadoras e voluntárias) na conferência useR! 2021 são obrigadas a concordar com este código de conduta. Relatos de violação a este Código de Conduta devem ser endereçados a useR2021-coc@r-project.org.

O código de conduta se aplica a todas as atividades da conferência, incluindo palestras, painéis, oficinas e eventos sociais. Ele se estende a todas as menções específicas à conferência nas redes sociais, por exemplo, postagens marcadas com o identificador da conferência (ex. #useR2021 no Twitter), respostas a tais postagens e respostas a contas oficiais de mídia social (ex. @_useRconf no Twitter).

A organização fará cumprir este código em sua totalidade e espera cooperação para garantir um ambiente seguro a todas as pessoas.

__Comportamento esperado__

Todas as pessoas participantes concordam em:

- Serem atenciosas na linguagem e nas ações e respeitar os limites das outras pessoas.
- Evitar linguagem e comportamentos humilhantes, discriminatórios ou assediadores. Por favor, consulte a seção Comportamento indesejado para mais detalhes.
- Alertar uma pessoa da equipe de respostas do Código de Conduta se notar alguém em perigo ou observar violações deste código de conduta, mesmo que pareçam irrelevantes. Por favor, consulte a seção “O que fazer se você testemunhar ou estiver sujeito a um comportamento inaceitável” para obter mais detalhes.

__Comportamento inaceitável__

O comportamento inaceitável inclui, mas não se limita a:

+ Perseguição, por exemplo, mensagens diretas indesejadas persistentes, envio de imagens indesejadas ou links de malware, registro de atividades online para fins de assédio.
+ Intimidação deliberada
+ Compartilhar conversas e discussões privadas (por exemplo, capturas de tela de canais de discussão, mensagens diretas etc.) sem consentimento.
+ Interrupção sustentada ou intencional de palestras ou outras atividades da conferência, incluindo discussões online.
+ Uso de imagens, comentários ou piadas sexuais ou discriminatórias.
+ Comentários ofensivos relacionados a idade, sexo, orientação sexual, deficiência, raça, religião, nacionalidade ou idioma nativo.
+ Contato físico simulado inadequado, por exemplo descrições textuais como “abraço” ou “backrub” ou emoji que representam tal contato, usadas sem consentimento.
+ Atenção sexual indesejada, o que pode incluir perguntas inapropriadas de natureza sexual, pedidos de favores sexuais ou pedidos repetidos de encontros ou informações de contato.

Se você for solicitado a interromper um comportamento inaceitável, deve parar imediatamente.

Pessoas que comportem de maneira inadequada estão sujeitas às ações listadas em Respostas às Violações do Código de Conduta.

__Requisitos adicionais para contribuições para conferências__

As apresentações não devem conter material ofensivo ou sexualizado. Se for impossível evitar esse material devido ao tema (por exemplo, extração de texto de material de sites de ódio). A existência desse material deve ser anotada no resumo e, no caso de contribuições orais, no início da palestra ou sessão.

__Requisitos adicionais para patrocinadores e contratantes de eventos sociais__

Patrocinadores e contratados de eventos sociais não devem usar imagens sexualizadas, atividades ou outro material. Eles não devem usar roupas / uniformes / fantasias sexualizados ou criar um ambiente sexualizado. Em caso de violação, podem ser aplicadas sanções, sem a devolução da contribuição de patrocínio.

__Respostas às violações do código de conduta__

A equipe de resposta do Código de Conduta reserva-se o direito de determinar a resposta apropriada para todas as violações do código de conduta. As respostas potenciais incluem:

+ Banir ou silenciar os espaços de conferência para impedir o comportamento impróprio.
+ Um aviso formal para interromper o comportamento de assédio.
+ Expulsão da conferência.
+ Remoção de monitores de patrocinadores ou apresentações em conferências
+ Cancelamento ou término antecipado de negociações ou outras contribuições para o programa.

Reembolsos não podem ser feitos em caso de expulsão.

__O que fazer se você testemunhar ou for alvo de um comportamento inaceitável__

Alerte a Equipe de Resposta ao Código de Conduta ou o Comitê Organizador se notar alguém em perigo, ou observar violações deste código de conduta, mesmo que pareçam irrelevantes.

As denúncias devem ser enviadas para useR2021-coc@r-project.org. Se a denúncia envolver alguém da equipe de resposta, esse membro e todos que entrarem em um conflito de interesse se afastarão do recebimento. Se você ainda tiver dúvidas sobre um conflito de interesses, entre em contato com o R Foundation Conference Committee em R-conferences@r-project.org.

Levaremos todos os reportes por useR! 2021 participantes a sério. Isso inclui incidentes fora de nossos espaços e em qualquer momento. O comitê organizador de useR! 2021 reserva-se o direito de excluir pessoas de useR! 2021 com base em seu comportamento anterior, incluindo comportamento fora de useR! 2021 espaços e comportamentos para com pessoas que não fazem parte de useR! 2021.

Respeitaremos os pedidos de confidencialidade com o objetivo de proteger os alvos de comportamento de assédio. Não nomearemos as vítimas sem seu consentimento afirmativo. A correspondência é tratada de forma confidencial e excluída após o caso ser resolvido. Todos os dados são armazenados de forma segura, o acesso será limitado aos membros da Equipe de Resposta do Código de Conduta que assumem o caso.

A equipe da conferência também fornecerá apoio às vítimas, incluindo, mas não se limitando a:

+ Instruir a equipe do evento para fornecer a assistência adequada.
+ Outra assistência para garantir que as vítimas se sintam seguras durante a conferência

A assistência só será fornecida com o consentimento afirmativo da vítima.

__Equipe de resposta do Código de Conduta__

- Adithi Upadhya
- Anicet Ebou
- Batool Almazrouq
- Danielle Smalls
- Muriel Buri
- Natalia Morandeira
- Paola Corrales
- Sara Mortara
- Gordana Popovic
- Andrea Sánchez-Tapia
- Rocío Joo

__Agradecimentos__

Partes do texto acima são licenciadas CC BY-SA 4.0. Crédito para SRCCON. Este código de conduta foi baseado no desenvolvido para useR! 2018, que foi uma revisão do código de conduta usado em conferências useR! anteriores e também baseado no código de conduta da rOpenSci.

