# Accessibility


The participation of people with disabilities in the conference is one of the key aspects of inclusion. Accessibility work requires planning in advance, and includes a wide array of practices. However, inclusion goes beyond accessibility and many of these practices also benefit all the attendants.

Be proactive about accessibility practices, don't expect people to request a specific type of access. This will create a genuinely inclusive space for everyone. Participants with invisible disabilities and neurodivergences should not have to disclose them to request for access. 

## A word about  language

Accessibility should not be framed as a liability or a burden. Do not follow an "accomodation" state-of-mind, where inclusion is a concession.

Do not frame disability as a matter of "special" "needs". Avoid euphemisms like "special" and "challenged" when talking about disability. Be matter of fact, and follow each individual's preference for how they identify their disability and what kind of help they need. 


## Tasks of the Accessibility team

The Accessibility team is in charge of: 

+ Ensuring the general [accessibility of the venue](#venue-accessibility) in in-person settings, including [inclusive practices around food](#inclusive-food)
+ Work with the technology team to make sure that the [online platforms](#platform-accessibility) used in the conference are accessible in any mode (virtual, in-person, or hybrid). This includes the website, the registration and submission platform, and the chat system
+ Promoting the [accessibility of the materials of the conference](#materials-accessibility), especially in online settings. This includes: 
  + defining, publishing, and promoting [Accessibility guidelines](#accessibility-guidelines) for presenters for every format of the conference
  + gathering and [making the materials of the conference available prior to the presentations](#materials-availability), to make them available to the audience who use screen readers, have low vision in in-person settings, or have low bandwith internet connection
+ The [captioning](#captioning), including optionally language interpretation and sign language interpretation, in any mode of the conference, and the final captioning and archiving of the conference materials
+ The [accessibility of the social events](#social-event-accessibility), in-person or virtual


## Accessibility of the venue {#venue-accessibility}

Choosing a venue for the conference is one of the earliest decisions and might be in the initial proposal. The venue should comply with accessibility standards, which in some countries are regulated by law. This means having areas not designed for conferences as venues, such as gyms, university food courts, may require aditional work around this topic. 

An accessible and inclusive venue should be at a walking distance to hotel(s) and other conference-related locations (food courts, social events spaces), and have accessible ground transportation options.
Some people with disabilities travel with assistants. These assistants should not have to pay to register and should have the points of contact information.

The sound system at the venue should have the audio induction loop system that can send signals directly to hearing aids. The availability of this system should be posted on a sign at the entrance to the room, along with specifications if only a certain part of the room is covered. At the time of presentations, everyone should talk into a microphone. If it's impractical to amplify people asking questions, the presenter or moderator should repeat the questions into the microphone. This is key for a hybrid format as well. 

Small breakout rooms should be located near the conference sessions and break areas to provide quiet places for conversations without background noise. There should be some seating available during breaks, even if most people will be standing. Consider visibly marking seats in meeting and poster rooms and break areas for use by disabled people.


Ensure that elevators, aisles, and pathways are wide enough for people using wheelchairs. Ensure that presenters using wheelchairs will be able to access the stage or podium.

Restrooms, including gender-neutral facilities, should be located near conference rooms and break areas. Information should be available on the locations of accessible bathrooms, break areas, lactation rooms, and meeting rooms.



###  Inclusive Practices around Food {#inclusive-food}

Provide a wide range of foods including gluten- and allergen-free, as well as vegetarian/vegan, options. Make ingredient lists available, and have someone from the catering service available for questions.

To avoid contamination with gluten or allergens at buffets, serve each food on separate plates or containers (e.g. do not contaminate cheese with crackers). Use the layout or decorations to discourage contamination of utensils. Place gluten- or allergen-containing foods later in the buffet so they don't contaminate allergen-free foods earlier in the sequence.

Lunch and break times should be long enough for people to leave the conference to get the food they need if it is not available at the conference.
Sugar free beverage options (diet soda, etc. and sweeteners like Stevia, Equal,
Sweet 'N' Lo) are needed at breaks, as well as plain water. 
If free alcohol is provided, have something other than beer and have non-alcoholic options.

Buffets, snacks, beverages, dining tables, and check-in tables should be within reach of someone using a wheelchair. 
At meals and breaks, someone should be available to help blind and low vision people to navigate food and beverage choices. Braille labels can be provided.



## Accessibility of the platforms used during the conference {#platform-accessibility}

+ The __conference website__ should be accessible to people with various disabilities as specified in the Web Content Accessibility Guidelines (WCAG; https://www.w3.org/WAI/standards-guidelines/wcag/). It should detail the inclusive practices that will be used at the conference so people can decide whether the conference will be accessible enough to attend. There should be a contact listed for questions about accessibility.

## Materials of the conference {#materials-accessibility}

Conference presentations and tutorials are made on both visual (slides) and auditory channels: the slides and the presenter's spoken words. For deaf, hard-of-hearing (HoH), blind, and low vision participants, integrating these two channels so that all information can be understood simultaneously can be difficult without inclusive practices.

Slides should be prepared avoiding the use of small fonts and attention given to choosing colors that are visible to people with color blindness. Recommendations can be found on the Perkins School for the Blind website:
https://www.perkinselearning.org/technology/digital-transitions/creating-accessible-powerpoint-presentations-students-visual

High-contrast color schemes are more visible.  Large print hard copies of talks could be more accessible for visually impaired participants.
Alt-text descriptions should be used for graphics, images, memes, screenshots,
and other graphically-presented material.


\ref{#accessibility-guidelines}
useR! 2021 prepared [accessibility guidelines](https://user2021.r-project.org/participation/accessibility/) for every format of the conference, and [a blog post](https://user2021.r-project.org/blog/2021/02/17/preparing-for-an-accessible-conference/) with details on how to prepare slide decks. 



## Availability of the materials {#materials-availability} 

In addition to making the presentation slide decks as accessible as possible, the materials should be available before the presentations, so that people who use screen readers, braille displays, and people who have low bandwidth and cannot use video during the conference can follow the details (e.g. code) of the presentation. 


Some presenters do not have finished materials in advance to the conference, but a stable link can be provided, so that the presenter can update their slides as needed, and attendants have access to the link in advance. 

The ideal format for slide decks in the context of useR! are markdown or beamer slides in html format, posted as a html. useR! 2021 created [instructions for making markdown or xaringan presentations available as GitHub or GitLab pages](https://gitlab.com/useR_2021/creating-github-gitlab-pages/-/tree/master) 




### During the presentations

Presenters should be guided to pace themselves so the audience can integrate both audio and visual information. Graphics, pictures, videos, and memes should be described audibly.

[Live captioning](#captioning) can provide access to spoken content for some participants. It could also be used to provide transcripts of talks later.
While some deaf/HoH people can use both sign language and captioning, others can only use one or the other.

Sign language interpreting is required for some potential participants. Sign langauge varies regionally, so for example when UseR! is in the United states, ASL (American sign language) interpreters should be available. For conferences in other locations, the local sign language would be relevant. Deaf participants should be able to sit near interpreters, and they should be able to watch the interpreter, the presenter, and the screen simultaneously. Interpreters must be available for social activities such as breaks and meals for full access to the conference environment.
The Environmental Science Department at St. Mary's University has a website with specific guidance for working with interpreters conferences and a variety of related situations:
https://smu.ca/academics/departments/environmental-science-work-with-interpreter.html

### After the presentations

Edit, caption, and upload Conference Materials

+ Previous consent for recording (talks and tutorials!)
+ Videos + Captions to R Consortium youtube - edit description

+ Add links to slides. Either self-generated link (gslides and github gitab pages) or link to a useR! conf unified repository

## Set up a point of contact {#accessibility-contact}

We recommend organizing a dedicated subteam for accessibility tasks. The team should designate points of contact and set up an email for answering any doubts regarding this topic. Include this email in the communication strategy of the conference.

## Social events accessibility {#social-event-accessibility}

Social events should be planned to be inclusive and safe to all people. This includes avoiding putting too much emphasis in movement, people having to talk, body types and abilities. For example, loud music at events can make it difficult for some to participate.