# A guide to hosting your first useR! Conference

__Project Title:__ useRknowledgebase

[Guide URL](https://rconf.gitlab.io/userknowledgebase) 


This useR! knowledgebase aims to be a manual with the cumulative knowledge useR! conference organizers have gathered during the editions of the conference, to guide future organizers and avoid them the need to "reinvent the wheel". 

It was started as part of the Google Season of Docs project in 2021.  This guide is work in progress, and our aim is that it will become the central and authoritative source of documentation on the useR! conference. 

By documenting thouroughy our steps, we also hope to be a useful guide to organizers of other conferences. 

## Milestones of the project 

- 30-Nov-2021 - First version of the useR! knowledgebase 
    - [x] A general structure of all the areas of the conference
    - [x] All the past documentation prepared by Forwards regarding Diversity, Inclusion, Accessibility and general good event practices
    - [x] Draft chapters with guides to organizing each area
    - [ ] A template to adapt existing chapters to a common format
    - [ ] A collaboration guide
- 2/Dec/2021 Discussion at the Global useR! Working Group to discuss future developments see issue 


## To get started

You can file an issue or comment any of the existing issues.

 <!-- Check our contributing guide for the style and format of each chapter.

You can reach Noa Tamir or [Andrea Sánchez-Tapia](mailto:andreasancheztapia@gmail.com) if you have any doubts. --> 

## Dependencies 

To build this book locally, you will need packages: __dplyr__, __kableExtra__, and __bookdown__


## To modify the content of this guide

1. Clone this git repo: 
`git clone https://gitlab.com/rconf/userknowledgebase`
2. The .Rmd documents at the root of the folder are the chapters for the guide. The images are in the `/assets` folder.
The structure of the guide is determined by the order in `_bookdown.yml`.
These are the only files you need to modify and commit/push
3. Gitlab C/I will build the version in branch `master` using the setup in file `.gitlab-ci.yml`
4. You can preview locally each chapter by knitting the document you are working on, but this does not update the rest of the files. 
5. To build a local version of the guide
    + Execute `bookdown::render_book("index.Rmd")` to build the `bookdown` site
    + Navigate to `/docs/` and open `index.html` on your browser


## Contributing

Corrections, suggestions, and general improvements are welcome as [issue submissions](https://gitlab.com/rconf/userknowledgebase/-/issues).

If you have write access, you can push directly to `master` for small fixes. Please use PRs to `master` for discussing larger updates - try to limit to one section or at least one chapter in each PR, so that changes are easier to review.
Please target your pull requests to the `master` branch.

## Background Resources

Details about this project can be found here [useR! Knowledgebase](https://github.com/rstats-gsod/gsod2021/wiki/useR!-knowledgebase).

## Acknowledgements

This book was started using Sean Kross' [minimal bookdown example](https://github.com/seankross/bookdown-start) as described on their [blog](http://seankross.com/2016/11/17/How-to-Start-a-Bookdown-Book.html).

This README borrowed ideas from [ropensci/dev_guide](https://github.com/forwards/first-contributions)

## Changelogs

### v21.05.01
* [Completed] Setting up the bookdown
* [Completed] Create issues for current milestone

### v21.06.01
* [Completed] Create the basic template of the ToC for review
* [TODO] Review with R GSoD Slack team member and Noa (Senior Writer)
* [TODO] Make improvements based on feedback gathered

## References

* [Previous Conferences](https://www.r-project.org/conferences/)
* [Bookdown homepage](https://bookdown.org/)
* [How to get started with Bookdown](https://bookdown.org/yihui/bookdown/get-started.html)
* [KnowledgeBase Project Idea](https://github.com/rstats-gsod/gsod2021/wiki/useR!-knowledgebase)
* [set of HOWTOs written by Forwards](https://github.com/forwards/conferences)
* [event best practices written by Forwards](https://github.com/forwards/event_best_practices)
* [blog post by the useR! 2021 diversity team](https://user2021.r-project.org/blog/2021/02/17/preparing-for-an-accessible-conference/)
* [satRdays knowledgebase](https://knowledgebase.satrdays.org/)
* [DISCOVER cookbook](https://discover-cookbook.numfocus.org/)
